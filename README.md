# Apollo EMP Documentation

[[_TOC_]]

Recommended EMP-fwk tag: v0.9.0-alpha1, both firmware and software verified


## Programming Command Module FPGAs


### Single FPGA (SVFplayer)

1. First load the CM firmware using uio_send
```bash
uio_send.py load-tar -f /fw/CM/fix_v300_Cornell_rev2_p1_VU13p-1-SM_USP.tar.gz
uio_send.py load-tar -f /fw/CM/Fix_Cornell_rev2_p2_VU13p-1-SM_USP.tar.gz
```
Make note of the client IDs generated for each as these are needed to unload the firmware at the end of your session.

When programming a single FPGA on the Command Module, `svfplayer`, within the `BUTool` suite may be used. This allows for programming over the command line from the SoM located on the card itself.

2. take the `ipbb`-generated package and download it onto the SoM. From there, unpack the package and move to the directory:
```bash
tar xvf <YOUR PACKAGE>.tgz
cd <YOUR PACKAGE>
```
3. Start `BUTool` to access `svfplayer`, then start programming:
```bash
BUTool.exe -a /opt/address_tables/connections.xml
```
```
cmpwrup
svfplayer <FILENAME>.svf PLXVC.XVC_1
```
where `<FILENAME>` is replaced by the name of the `.svf` file within the package.

The programming repeats for four times, then should complete without errors.

This step is possible for both FPGAs, just replace the .svf with the correct svf for each FPGA seperately. 

4. Clear the AXI within `BUTool` with:

```
unblockAXI
```

5. Check for errors from within `BUTool` with the `status` command.

If the programming has been successful, the status should look like this:
```
              C2C| AXILITE_AR_READY_TIMEOUT| CM_1 C2C_1| CM_1 C2C_2| CM_2 C2C_1| CM_2 C2C_2|
               --|-------------------------|-----------|-----------|-----------|-----------|
            CM1_K|                        1|           |           |           |           |
            CM1_V|                        1|           |           |           |           |
  ENABLE_PHY_CTRL|                         |          1|          1|          1|          1|
        LINK_GOOD|                         |          1|          1|          0|          0|
    PHYLANE_STATE|                         |    RUNNING|    RUNNING|   initWAIT|   initWAIT|
      PHY_LANE_UP|                         |          1|          1|          0|          0|
```

6. Test the FPGA status with `NUTool`
```
cd /home/cms/khahn/NUTools/
source scripts/env.sh
cd -
export NU_TOOLS_XML="file://<PATH TO DIRECTORY CONTAINING connection.xml FILE>"
regtool.exe -dapollo.c2c.vu13p -rinfo.versions
```
Expected output (exact value of register may differ):
```
device: apollo.c2c.vu13p
reg: info.versions
---------------------------
info.versions : 0x13000705
```

7. Unload the FW
At the end of your session unload the FW using uio_send
```bash
uio_send.py unload-tar -u CLIENT_ID
```
Where CLIENT_ID is the ID generated when you loaded the FW

### Dual FPGA Programming (Vivado HW manager). Depreciated as of emp-fwk tag v0.9.0-alpha1

To programme both FPGAs on the Command Module, `Vivado Hardward Manager` within `Vivado` may be used.

1. Login to `imperialdaq002` in a way that allows you to use the GUI.

2. Source vivado
```
source /opt/Xilinx/Vivado/2021.2/settings64.sh
```
and launch vivado
```
vivado
```
3. From the vivado GUI select `open hardware manager`

4. From the opened page select `open target` then `autoconnect`

5. In the side bar under hardware right-click `localhost` and select `Add XVC`. In the opened pop-up add the host name of the CM e.g. `CMSAPOLLO214.cern.ch`

6. Then power on the CM in BUTool on the desired board with
```bash
BUTool.exe -a /opt/address_tables/connections.xml
```
7. From within BUTool
```
cmpwrdown
cmpwrup
```

8. Within vivado the two FPGAs should have appeared, program them by right clicking the FPGA and selecting `program device` selecting the bit file you would like to load onto the FPGA.

### Extra Steps after Power Cycle of Board

If the whole board has been powered off, some extra steps must be made, primarily to configure the Service Module to send the backplane clock to the Command Module.

#### Service Module TCDS Select
To use the tcds clock source for FPGA2 in `BUTool` run these commands:
```
write SERV.CLOCKING.HQ_SEL 0
write SERV.TCDS.TTC_SOURCE 0
```
Reset the clock source of FPGA1 to TCDS2 then FPGA2 to TCDS2

#### R1B Clock Synth

1. Disable uC logging via minicom on the SM
```bash
minicom -c on  -D /dev/ttyUL1
```
```
log mon err
```
This will stop the continuous errors being printed in the console. Disconnect with `ctrl+a q`.

2. Load the R1B synth
```bash
cd ~/khahn/tmp/Cornell_CM_Rev2_HW/ClockBuilderPRO/programs/
python LoadSynth.py r1b ../projects/Si5395-RevA-R1Bv0006-Registers.h --tty ttyUL1
```


## Link Testing

First program the FPGAs then set up the CM for TCDS2. From within BUTool
```
write SERV.CLOCKING.HQ_SEL 0
write SERV.TCDS.TTC_SOURCE 0
```

Then 
```bash
empbutler -c CONNECTIONS_FILE.xml do apollo.c2c1.vu13p reset tcds2
empbutler -c CONNECTIONS_FILE.xml do apollo.c2c2.vu13p reset tcds2
```
Note the device `apollo.c2c1.vu13p` and `apollo.c2c2.vu13p` where c2c1 refers to FPGA1 and c2c2 refers to FPGA2

First check the ref clocks for the FPGA are correct
```bash
empbutler -c CONNECTIONS_FILE.xml do apollo.c2c2.vu13p mgts measure-clocks -c CHANNEL_LIST
```
Where CHANNEL_LIST is the links you want to test, avoid running over all links as this takes a long time

If the clock ref is not:
```
Async ref : 320 MHz
```
See common issues to reprogram the clocks.

On the TX board:
```bash
empbutler -c CONNECTIONS_FILE.xml do apollo.c2c2.vu13p buffers rx PlayOnce -c CHANNEL_LIST --inject generate:counter-with-index
```
Or your own file in the --inject (see [emp documentation](https://serenity.web.cern.ch/serenity/emp-fwk/software/butler/buffers.html) for details)

Next configure the MGTs
```bash
empbutler -c CONNECTIONS_FILE.xml do apollo.c2c2.vu13p mgts configure tx -c CHANNEL_LIST
```

On the RX board:
```bash
empbutler -c CONNECTIONS_FILE.xml do x0 reset tcds2
empbutler -c CONNECTIONS_FILE.xml do x0 mgts configure rx -c CHANNEL_LIST
empbutler -c CONNECTIONS_FILE.xml do x0 mgts align -c CHANNEL_LIST
empbutler -c CONNECTIONS_FILE.xml do DEVICE_ID buffers rx Capture -c CHANNEL_LIST
empbutler -c CONNECTIONS_FILE.xml do DEVICE_ID capture --rx CHANNEL_LIST --tx CHANNEL_LIST
```

Current Apollo -> Serenity 4+4 Apollo channels 84-87 are connected to Serenity 68-71


## Common Issues

**Problem:** My empbutler commands give me:
```
 ERROR - No matching files for expression "/home/cms/<myfolder>/apollo_cm_v2_vu13p_p1/addrtab/top_emp_apollo.xml" with parent path "/home/cms/<myfolder>"
 ```
**Solution:**
Check the paths supplied in CONNECTIONS_FILE.xml  match your unpacked build folders, e.g.
```bash
<connections>
 <connection id="apollo.c2c1.vu13p" uri="ipbusmmap-2.0:///dev/uio_F1_IPBUS?offset=0x0" address_table="file:///home/cms/<myfolder>/<mypackage>/addrtab/top_emp_apollo.xml" />
 <connection id="apollo.c2c2.vu13p" uri="ipbusmmap-2.0:///dev/uio_F2_IPBUS?offset=0x0" address_table="file:///home/cms/<myfolder>/<mypackage>/addrtab/top_emp_apollo.xml" />
</connections>

```


**Problem:** My reset tcds2 command gives me:
```
uhal._core.exception: SIGBUS received during 16-byte read @ 0x0 in /dev/uio_F2_IPBUS
```

**Solution:** In BUTool
```
unblockAXI
```

**Problem:** My reset tcds2 command gives me a clock not locked error
**Solution:** Reconfig the dth
Login via imperialdaq002 or one of the boards, ask someone for the password
```bash
 ssh DTH@10.0.0.22
```
On the DTH
```bash
tcds2_dth_driver reload test
tcds2_dth_driver id
```
This should show:
```
System ID               : TCDS2
Board ID                : DTH_P1V2
Function ID             : DTH
Firmware version        : 0.1.0
Firmware build timestamp: 20220208T211234
```
Then:
```
tcds2_dth_driver configure 
tcds2_dth_driver sfps enable 
tcds2_dth_driver links init
```

If this doesn't work rerun:
```
tcds2_dth_driver sfps enable 
tcds2_dth_driver links init
```

Sometimes the DTH firmware needs to be reflashed if the firmware revision doesn't match the above. Run the following on the DTH:
```
tcds2_dth_driver fw program test /home/DTH/mp801/dth-hack/tcds-dth-hack.bin
tcds2_dth_driver reload test 
tcds2_dth_driver reload id 
```

**Problem:** My reset tcds2 command gives me:
```
emp._emp_python.exception: TX and RX PLLs in TCDS2 interface not locked
```

**Solution:** Reboot the board
Within the cern network
```
telnet cmsapollo214ipmc

restart
```

**Problem:**  My Ref Clock does not show 320 MHz

**Solution:** To reprogram the eeproms first install [mcu-tools](https://github.com/apollo-lhc/mcu_tools/tree/49550e8528f47c6110fffe0f31d86d0791471470)

Then from within the mcu-tools directory
```bash
python LoadClkConfig_tripletEEPROM.py R0A Si5341-RevD-R0Av0006-Registers.txt --tty ttyUL1 --quiet
```
If there is an error change:
```
ord(line[0]) to line[0]
```
in line 69 of LoadClkConfig_tripletEEPROM.py

Then reboot the board

A fast way is to load the clock from within butool:
```
uart_cmd CM_1 clkmon 3
uart_cmd CM_1 loadclock 3
```

**Problem:** My MGTs configure command gives me:
```
Error: [LinkProtocolError] Serial bitstream received on channels [68] is inconsistent with CSP. Packet counter non-zero, polarity could be wrong.
```
**Solution:**
- Add an `--invert` to the configure command
- Check the ref clock
- Check the links are correct

**Problem:** My `source emp-toolbox/scripts/env.sh` command give me  
```
RuntimeError: Click will abort further execution because Python was configured to use ASCII as encoding for the environment. Consult https://click.palletsprojects.com/unicode-support/ for mitigation steps.
```
**Solution:**
Use the following:
```
export LC_ALL=en_US.utf-8
export LANG=en_US.utf-8
```
**Problem:** 
The `cmpwrup`` command gives:
```
CM 1 failed to powered up in time
```
**Solution:**
Reboot the board

If that doesn't work, ask the slack channel

### Bonus Commands
Some useful commands in BUTool

```
status
```
Prints out a load of info about the CM. If the LINKs show many errors, reboot the board `sudo reboot -n`

```
uart_cmd CM_1 ff xmit on all
```
Turn on the lasers in the FFs
```
uart_cmd CM_1 ff cdr on all
```
Turn on CDR in the FFs

```
uart_cmd CM_1 clkmon 0
```
Read clock info of the CM, useful for checking which clock config is loaded

To change the FF equalisation settings:
```
ff regw REGISTER_ADDR VALUE FF_NUMBER
```
with REGISTER_ADDR going from 0x3e -- 0x43


e.g.
```
ff regw 3e 1 4
```

